**4-20mA Current loop**

![4-20mA_current_loop_components](https://www.predig.com/sites/default/files/images/Indicator/back_to_basics/4-20mA_Current_Loops/4-20mA_current_loop_components.jpg)
1. Sensor
First, there needs to be some sort of sensor which measures a process variable. A sensor typically measures temperature, humidity, flow, level or pressure. The technology that goes into the sensor will vary drastically depending on what exactly it is intended to measure, but this is not relevant for this discussion.

2. Transmitter
Second, whatever the sensor is monitoring, there needs to be a way to convert its measurement into a current signal, between four and twenty milliamps. This is where a transmitter will come into play. If, for instance, a sensor was measuring the height of a fifty foot tank, the transmitter would need to translate zero feet as the tank being empty and then transmit a four milliamp signal. Conversely, it would translate fifty feet as the tank being full and would then transmit a twenty milliamp signal. If the tank were half full the transmitter would signal at the halfway point, or twelve milliamps.

3. Power Source
In order for a signal to be produced, there needs to be a source of power, just as in the water system analogy there needed to be a source of water pressure. Remember that the power supply must output a DC current (meaning that the current is only flowing in one direction).

There are many common voltages that are used with 4-20 mA current loops (9, 12, 24, etc.) depending on the particular setup. When deciding on what voltage of power supply to use for your particular setup, be sure to consider that the power supply voltage must be at least 10% greater than the total voltage drop of the attached components (the transmitter, receiver and even wire). The use of improper power supplies can lead to equipment failure.

4. Loop
In addition to an adequate VDC supply, there also needs to be a loop, which refers to the actual wire connecting the sensor to the device receiving the 4-20 mA signal and then back to the transmitter. The current signal on the loop is regulated by the transmitter according to the sensor's measurement. This component is typically overlooked in a current loop setup because wire is so intrinsic to any modern electronic system, but should be considered in our exploration of the fundamentals. While the wire itself is a source of resistance that causes a voltage drop on the system, it is normally not a concern, as the voltage drop of a section of wire is minuscule. However, over long distances (greater than 1,000 feet) it can add up to a significant amount, depending on the thickness (gauge) of the wire.

5. Receiver
Finally, at someplace in the loop there will be a device which can receive and interpret the current signal. This current signal must be translated into units that can be easily understood by operators, such as the feet of liquid in a tank or the degrees Celsius of a liquid. This device also needs to either display the information received (for monitoring purposes) or automatically do something with that information. Digital displays, controllers, actuators, and valves are common devices to incorporate into the loop.

These components are all it takes to complete a 4-20 mA current loop. The sensor measures a process variable, the transmitter translates that measurement into a current signal, the signal travels through a wire loop to a receiver, and the receiver displays or performs an action with that signal.


**MODBUS**

![How-does-Modbus-Communication-Protocol-Work-Between-Devices-1](uploads/f151d1ec6a9a6570ead540732f0ec4e4/How-does-Modbus-Communication-Protocol-Work-Between-Devices-1.png)

![Modbus-Plant](https://i0.wp.com/habrastorage.org/webt/bb/c7/yu/bbc7yu_iqs8ucr9ofnja0lv48fm.png?w=500&ssl=1)

Modbus is a data communications protocol originally published by Modicon (now Schneider Electric) in 1979 for use with its programmable logic controllers (PLCs). Modbus has become a de facto standard communication protocol and is now a commonly available means of connecting industrial electronic devices.
Modbus is often used to connect a plant/system supervisory computer with a remote terminal unit (RTU) in Supervisory Control and Data Acquisition (SCADA) systems in the electric power industry. Many of the data types are named from industrial control of factory devices, such as Ladder logic because of its use in driving relays: A single physical output is called a coil, and a single physical input is called a discrete input or a contact.

* Modbus is a communication protocol for use with programmable logic controllers (PLC). It is typically used to transmit signals from instrumentation and control devices back to a main controller, or data gathering system.
* The method is used for transmitting information between electronic devices. The device requesting information is called “master” and “slaves” are the devices supplying information.
* In a standard Modbus network, there is one master and up to 247 slaves, each with a unique slave address from 1 to 247.
* Communication between a master and a slave occurs in a frame that indicates a function code.
* The function code identifies the action to perform, such as read a discrete input; read a first-in, first-out queue; or perform a diagnostic function.
* The slave then responds, based on the function code received.
* The protocol is commonly used in IoT as a local interface to manage devices.
* Modbus protocol can be used over 2 interfaces
  1. RS485 - called as Modbus RTU
  1. Ethernet - called as Modbus TCP/IP

### What is RS485?

* RS485 is a serial (like UART) transmission standard, you can put several RS485 devices on the same bus.
* RS485 is not directly compatible: you must use the correct type of interface, or the signals won't go through. Mainly done through an easy to use an RS485 to USB.

**OPC UA SERVER**

![OPC-UA-Server-for-Databases](https://docs.faircom.com/doc/c-treeEDGE_DevelopmentGuide/1905model_diagrams_opc_detail.png)

**MQTT**

![Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol](https://miro.medium.com/max/5080/1*7MwXy5N4rx4mAxZ2KZrwJQ.png)

**HTTP**

![http-req-res](https://gitlab.com/iotiotdotin/project-internship/iotmodule2/-/wikis/uploads/85f443d40e36e1f5bcb4e71ac14205b0/http-req-res.png)