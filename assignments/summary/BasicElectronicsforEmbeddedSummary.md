**Sensors:** Devices which convert useful energy signals to electrical signals. Converting into Electrical signals is done as these signals are very easy to transmit. 
               There are different types of sensors. water level sensor, temperature sensor, light sensor, moisture sensor, gyroscope sensor to name a few.

![](https://5.imimg.com/data5/XP/RS/MY-9380557/sensors-500x500.jpg)

**Actuators:**  Devices which convert electrical signals to useful energy signals. Without actually knowing that those are called actuators we mostly use many actuators those are fans, electric bulbs, mixers, Motors, Linear actuators, relays so on and so forth.

![actuators](file:///D:/Mantis/internship/IOTIOT.IN/images/actuators.jpg)

   **Sensors and actuators belong to the primary division of an IOT architecture. 

**ANALOG AND DIGITAL**

**Type of Data**
Analog signals are continuous in nature, whereas digital signals are discrete.

**Type of Waves**
Analog signal wave type is sinusoidal, whereas a digital signal is a square wave.

**Medium of Transmission**
Analog signal medium of transmission is wire or wireless, whereas a digital signal is a wire.

**Type of Values**
Analog signal value type if positive as well as negative, whereas a digital signal is positive.

**Security**
The security of an analog signal is not encrypted, whereas a digital signal is encrypted.

**Bandwidth**
The analog signal bandwidth is low, whereas the digital signal is high.

**Hardware**
Analog signal hardware is not elastic, whereas digital is elastic in execution

**Data Storage**
The data storage of an analog signal is in the wave signal form, whereas digital signal stores the data in the binary bit form.

**Portability**
Analog signals are portable similar to the thermometer and low cost, whereas digital signals are portable similar to computers and expensive.

**Data transmission**
In analog, the signal can be deterioration due to noise throughout transmission, whereas digital signal can be noise resistant throughout transmission devoid of any deterioration.

**Impedance**
The impedance of the analog signal is low, whereas the digital signal is high.

**Power Consumption**
Analog devices use more power, whereas digital devices use less power.

**Data Transmission Rate**
The data transmission rate in the analog signal is slow, whereas in the digital signal it is faster.

**Examples**
The best examples of the analog signal are video, human voice in the air, radio transmission waves or TV
transmission waves.

**Applications**
Analog signals can be utilized in analog devices exclusively, thermometer, whereas digital signals are appropriate for digital electronic devices like computers, PDA, cell phones.

![images]()



**MICRO PROCESSORS AND MICRO CONTROLLERS**

![](https://electronicsforu.com/wp-contents/uploads/2016/05/Screen-Shot-2016-05-28-at-16.51.34.png)

- Arduino, intel 8051 are Micro controller.
- Rasberry Pi, Intel 8085, 8086 are Micro processors.
     Currently these products are ruling the semi conductor market.


**RASBERRY PI**

The Raspberry Pi is a low cost, credit-card sized computer that plugs into a computer monitor or TV, and uses a standard keyboard and mouse. It is a capable little device that enables people of all ages to explore computing, and to learn how to program in languages like Scratch and Python. It’s capable of doing everything you’d expect a desktop computer to do, from browsing the internet and playing high-definition video, to making spreadsheets, word-processing, and playing games.

What’s more, the Raspberry Pi has the ability to interact with the outside world, and has been used in a wide array of digital maker projects, from music machines and parent detectors to weather stations and tweeting birdhouses with infra-red cameras. We want to see the Raspberry Pi being used by kids all over the world to learn to program and understand how computers work.

The Raspberry Pi 3 Model B uses a Broadcom BCM2837 SoC with a 1.2 GHz 64-bit quad-core ARM Cortex-A53 processor, with 512 KiB shared L2 cache. The Model A+ and B+ are 1.4 GHz

The Raspberry Pi 4 uses a Broadcom BCM2711 SoC with a 1.5 GHz 64-bit quad-core ARM Cortex-A72 processor, with 1 MiB shared L2 cache.

**Raspberry Pi 4** Model B was released in June 2019 with a 1.5 GHz 64-bit quad core ARM Cortex-A72 processor, on-board 802.11ac Wi-Fi, Bluetooth 5, full gigabit Ethernet (throughput not limited), two USB 2.0 ports, two USB 3.0 ports, and dual-monitor support via a pair of micro HDMI (HDMI Type D) ports for up to 4K resolution . The Pi 4 is also powered via a USB-C port, enabling additional power to be provided to downstream peripherals, when used with an appropriate PSU. The initial Raspberry Pi 4 board has a design flaw where third-party e-marked USB cables, such as those used on Apple MacBooks, incorrectly identify it and refuse to provide power. Tom's Hardware tested 14 different cables and found that 11 of them turned on and powered the Pi without issue. The design flaw was fixed in revision 1.2 of the board, released in late 2019.


![1024px-Raspberry_Pi_4_Model_B_-_Side](https://upload.wikimedia.org/wikipedia/commons/f/f1/Raspberry_Pi_4_Model_B_-_Side.jpg)

***Rasberry pi applications are so vast that making a summary is difficult.

**SERIAL AND PARALLEL COMMUNICATION**

![Untitled](https://miro.medium.com/max/1103/0*Hi44DwHAAkm7Jh_q.gif)

The crucial difference between serial and parallel communication is that in serial communication a single communication link is used to transfer the data from an end to another. As against in parallel communication, multiple parallel links are used that transmits each bit of data simultaneously.

**Key Differences Between Serial and Parallel Communication**

- Due to the presence of single communication link the speed of data transmission is slow. While multiple 
  links in case of parallel communication allows data transmission at comparatively faster rate.
  Whenever there exists a need for system up-gradation then upgrading a system that uses serial 
  communication is quite an easy task as compared to upgrading a parallel communication system.
- In serial communication, the all data bits are transmitted over a common channel thus proper spacing is 
  required to be maintained in order to avoid interference. While in parallel communication, the 
  utilization of multiple link reduces the chances of interference between the transmitted bits.
- Serial communication supports higher bandwidth while parallel communication supports comparatively 
  lower bandwidth.
- Serial communication is efficient for high frequency operation. However, parallel communication shows 
  its suitability more in case of low frequency operations.
- Due to existence of single link, the problem of crosstalk is not present in serial communication. But 
  multiple links increase the chances of crosstalk in parallel communication.
- Serial communication is suitable for long distance transmission of data as against parallel 
  communication is suitable for short distance transmission of data.
